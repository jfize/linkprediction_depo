# coding = utf-8
import argparse
import time
import glob

from lib.erosion_model import  eval_erosion_model
from lib.utils import load_edgelist

import pandas as pd
import networkx as nx

from joblib import Parallel,delayed
from tqdm import tqdm


parser = argparse.ArgumentParser()
parser.add_argument("graph_dir")
parser.add_argument("output_filename")
parser.add_argument("-f", "--format", default="gml", choices=["gexf", "gml", "txt"])
parser.add_argument("-t","--train-frac",default=0.9,type=float)
parser.add_argument("-n","--nb-iteration",default=1,type=int)
parser.add_argument("-v","--verbose",action="store_true")


args = parser.parse_args()
fns = sorted(glob.glob(args.graph_dir + "/*." + args.format))

NB_ITERATION = args.nb_iteration

data = []
def evaluate(fn):
    if args.format == "txt":
        G = load_edgelist(path=fn)
    elif args.format == "gml":
        G = nx.read_gml(fn)
    else:
        G = nx.read_gexf(fn)
    G = nx.convert_node_labels_to_integers(G)
    auc_sbm,auc_spat,auc_our_model = eval_erosion_model(G,NB_ITERATION)
    nb_edges = G.size()
    nb_nodes = len(G)
    nb_com = G.graph["nb_com"]
    alpha = G.graph["alpha"]
    return [nb_nodes,nb_edges,nb_com,alpha,fn,auc_sbm,auc_spat,auc_our_model]


deb = time.time()
data = Parallel(n_jobs=4,backend="multiprocessing")(delayed(evaluate)(fn) for fn in tqdm(fns))
#data =[evaluate(fn) for fn in tqdm(fns)]
print("eval took",time.time()-deb)

df = pd.DataFrame(data,columns="nb_nodes nb_edges nb_com alpha fn auc_sbm auc_spat auc_our_model".split())
df.to_csv(args.output_filename,sep="\t")


