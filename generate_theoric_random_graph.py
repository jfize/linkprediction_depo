# coding = utf-8
import itertools
import os
import argparse

import lib.random as ra

import networkx as nx
from tqdm import tqdm


# COMMAND PARSING
parser = argparse.ArgumentParser()
parser.add_argument("output_dir")
args = parser.parse_args()


GRAPH_SIZE = [80,800,5000]
EDGE_FACTOR = [2,4,5,10] # the number of edges is computed by multiplicating the edge factor with the number of nodes.
sample_per_params  = 10

OUTPUT_DIR = args.output_dir
if not os.path.exists(OUTPUT_DIR):
    os.makedirs(args.output_dir)


parameters = {
    "stochastic_block_model_graph": {
        "nb_nodes":GRAPH_SIZE,
        "nb_edges":EDGE_FACTOR,
        "nb_com" :[2,5,8,16,10,25],
        "percentage_edge_betw":[0.1,0.01]
    },
    "ER_graph": {
        "nb_nodes":GRAPH_SIZE,
        "nb_edges":EDGE_FACTOR
    },
    "powerlaw_graph": {  # configuration_model
        "nb_nodes":GRAPH_SIZE,
        "nb_edges":EDGE_FACTOR,
        "exponent":[2,3],
        "tries":[100]
    },
    "spatial_graph":{
        "nb_nodes":GRAPH_SIZE,
        "nb_edges":EDGE_FACTOR,
        "coords":["random","country"],
    },
    "mixed_model_spat_sbm":{
        "nb_nodes":GRAPH_SIZE,
        "nb_edges":EDGE_FACTOR,
        "nb_com":[2,4,8,16],
        "alpha":[0,0.01,0.1,0.5,0.7,1]

    }
}

def get_params(inp):
    return (dict(zip(inp.keys(), values)) for values in itertools.product(*inp.values()))

pbar = tqdm(parameters.items(),total=len(parameters))
for method,args in pbar:
    pbar.set_description("Generating graphs using : " + method)
    list_of_params = get_params(parameters[method])
    func = getattr(ra,method)
    for ix,params in enumerate(list_of_params):
        params["nb_edges"] = params["nb_edges"] * params["nb_nodes"]
        print("Gen graph using the following parameters : ",params)
        for sp_id in range(sample_per_params):
            try:
                G = func(**params)
                G.graph.update(params)
                nx.write_gml(G, OUTPUT_DIR+"/graph_{method}_{ix}_{sp_id}.gml".format(method=method,ix=ix,sp_id=sp_id),stringizer=str)
            except Exception as e:
                print(e)
                print("Can't generate graphs using these parameters")


