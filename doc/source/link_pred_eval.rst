Link Prediction Evaluation
==========================

Link Prediction with state of the art methods
---------------------------------------------

In order to evaluate link predictions methods on a graph dataset, we use the `run_eval.py` script. Here
a graph dataset is a dirctory that contains a file for each graph. For now, the accepted graph format are
: edgelist (txt), gephi (.gexf) and graphml (.gml).

To launch the evaluation, just run the following command in your terminal:

.. code-block::shell

    python run_eval.py <dataset_dir_path> <output_filename> [-f graph_format] [-t edge fraction used] [-v verbose]


Select which link prediction methods to evaluate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can change the methods evaluated in the `evalNE_script.py` file by changing different variables.
To change the heuristics evaluated, change the following :

.. code-block:: python

    methods_heuristics = ['random_prediction',
        'common_neighbours',
        'jaccard_coefficient',
        "adamic_adar_index",
        "preferential_attachment",
        "resource_allocation_index",
        "stochastic_block_model",
        "stochastic_block_model_degree_corrected",
        "spatial_link_prediction"
               ]

The string corresponds to the name of the function available in `evalne.methods.similarity` modules.

Thanks to **OpenNE**, we can also evaluated methods based on networks embedding. To remove or add such methods
modify the following variables in the same file :

.. code-block:: python

    methods_ne = ["node2vec" ,"hope-opne", "gf", "sdne", "deepWalk", "line", "grarep"]
    commands = [
        "python -m openne --method node2vec --graph-format edgelist --epochs 100 --number-walks 10 --walk-length 80 --window-size 10",
        "python -m openne --method hope --epochs 100",
        "python -m openne --method gf --epochs 100",
        "python -m openne --method sdne --epochs 100 --encoder-list [1024,128] --beta 5 --bs 500",
        "python -m openne --method deepWalk --graph-format edgelist --epochs 100 --number-walks 10 --walk-length 80 --window-size 10",
        "python -m openne --method line --graph-format edgelist --epochs 10",
        "python -m openne --method grarep --epochs 100"
        ]

More specifically, in `methods` you add the name of the one method you want to use; and in `commands`, you add the openne
command required to compute the embedding using this method.

Link prediction based on the erosion model
------------------------------------------

To run the evaluation of our link prediction method based on erosion, you must use the `eval_mixed_model.py` script.
