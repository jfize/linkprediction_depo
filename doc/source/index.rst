.. Link Prediction documentation master file, created by
   sphinx-quickstart on Tue May  4 13:26:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Link Prediction's documentation!
===========================================

Summary
-------
.. toctree::
   :maxdepth: 2

   installation
   get_started.ipynb
   graph_generator
   link_pred_eval






Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
