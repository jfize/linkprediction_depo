

Graph Generators
===================

All graph generator can be found in the module `lib.random`. For every graph generator, you can set the number of edges and nodes
in the resulting graph.

For example, if you want to generate a graph following the stochastic block model, use the follwing code :

.. code-block:: python

    from lib.random import stochastic_block_model_graph
    G = stochastic_block_model_graph(nb_nodes=300,nb_edges=1200,nb_com=5,percentage_edge_betw=0.01)

Graph Models
------------

Stochastic Block Model
^^^^^^^^^^^^^^^^^^^^^^
This model partitions :math:`n` vertices in :math:`k` blocks, and places edges between pairs of nodes with a probability that depends on the vertices membership. The probability of a pair of nodes is computed based four parameter, the number of vertices :math:`|V|` and edges :math:`|E|`, the number of blocks :math:`|B|` and finally, the percentage of interlinks :math:`per_{betw}`, i.e number of selected pairs of nodes that belongs to different blocks.

First, we assign randomly a block :math:`b_i \in B` for each node :math:`v \in V`. In this model, all blocks are associated with the same number of nodes :math:`\frac{|V|}{|B|}`. Second, to compute the probability of a pair of nodes to be connected, we use the function :math:`f(u,v)` defined as follows:

.. math::

    f(u,v)=\begin{cases}
           p_{in}, & if b_u == b_v\\
           p_{out}, & otherwise
     \end{cases}


where :math:`p_{in}` is the probability of a pair of nodes from the same block to be connected, and :math:`p_{out}` is the probability of a pair of nodes with different block membership to be connected. :math:`b_u` and :math:`b_v` correspond the block membership for the nodes :math:`u` and :math:`v`.


To compute the :math:`p_{in}` and :math:`p_{out}`, we do the following:

.. math::

    \begin{eqnarray}
    U_{in} = \sum\limits_{b\in B} \frac{N_b\times(N_b-1)}{2}\\
    U = \frac{|V| \times(|V| -1)}{2}\\
    U_{out} = U-U_{in}\\
    L_{out} = |E| * per_{betw}\\
    L_{in} = |E| - L_out\\
    \\p_{in} = L_{in}/U_{in}\\
     p_{out} = L_{out}/U_{out}
    \end{eqnarray}


Spatial Model
^^^^^^^^^^^^^
This model generate a graph with $n$ vertices and $e$ edges selected randomly. Edges are selected using the deterrence function defined in the following formula:

.. math::
    err(u,v) = \frac{1}{|p_u - p_v|^2 +\epsilon}

where :math:`u` and :math:`v` are two vertices and :math:`p_u` and :math:`p_v` correspond to their position.

Nodes coordinates can be generated randomly or randomly selected from existing places (here countries centroids).

ER Random Graph
^^^^^^^^^^^^^^^
The model returns a Erdős-Rényi graph or a binomial graph. Basically,
the model generates a graph :math:`G_{n,m}` where :math:`n` corresponds to the number of vertices
and :math:`m` the number of edges in :math:`G_{n,m}`. In this model, each pair of nodes has the same
probability to be connected.

Configuration model
^^^^^^^^^^^^^^^^^^^

The configuration model generates a graph (graph with parallel edges and self loops) by randomly assigning edges to match a given degree distribution. In our case, generated graph degree distribution follows a powerlaw. We use the Molloy-Reed approach [molloy1995critical]_  to generate the graph.

.. [molloy1995critical] Molloy, M., & Reed, B. (1995). A critical point for random graphs with a given degree sequence. Random structures & algorithms, 6(2‐3), 161-180.


Generate a graph dataset based on different models and configurations
---------------------------------------------------------------------

If you wish to generate a dataset containing generated graph with different configurations,
you can use the script generate_random_graph.py` using the following command :

.. code-block:: shell

    python generate_theoric_random_graph.py <output_dir>


You can modify the parameters of each configuration for each model in the script source code.

Graph parameters
^^^^^^^^^^^^^^^^

.. code-block:: python

    GRAPH_SIZE = [80,800,5000]
    EDGE_FACTOR = [2,4,5,10]
    sample_per_params  = 10

Model parameters
^^^^^^^^^^^^^^^^

.. code-block:: python

    parameters = {
    "stochastic_block_model_graph": {
        "nb_nodes":GRAPH_SIZE,
        "nb_edges":EDGE_FACTOR,
        "nb_com" :[2,5,8,16,10,25],
        "percentage_edge_betw":[0.1,0.01],
        },
        #...
    }


API Reference
-------------
.. automodule:: lib.random
   :members: