Installation
============

To simplify things, this installation guide is using conda environnement. Therefore, first, we must
create a Python3.7 environnement

.. code-block:: shell

    conda create --name test_lp python=3.7
    conda activate test_lp

Then, you need to install graph-tool.

.. code-block:: shell

    conda install -c conda-forge graph-tool

Once graph-tool is installed, we need to install OpenNE:

.. code-block:: shell

    git clone https://github.com/thunlp/OpenNE.git
    cd OpenNE
    conda install numpy networkx scipy tensorflow==1.14 gensim scikit-learn
    cd src
    python setup.py install
    cd ..

To verify if the installation went well, run the following command :

.. code-block:: shell

    python -m openne --help

Then, install our custom version of EvalNE using pip

.. code-block:: shell

    pip install git+https://github.com/Jacobe2169/EvalNE


Finally, To use the code in this repo, you'll need to install Python requirements using the following command

.. code-block:: shell

    pip install -r requirements.txt
    pip install decorator==4.3