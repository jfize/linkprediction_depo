# coding = utf-8
import numpy as np

euclid_dist = lambda a,b : np.linalg.norm(a-b)**2
hash_func = lambda x:"_".join(sorted([str(x[0]),str(x[1])]))