# coding = utf-8
import glob
import re
import argparse
import os

import pandas as pd
import numpy as np
import networkx as nx

import matplotlib.pyplot as plt
import seaborn as sns


parser = argparse.ArgumentParser()
parser.add_argument("graph_directory")
parser.add_argument("output_directory")
parser.add_argument("number_of_files",type=int)

args = parser.parse_args()
graph_dir = args.graph_directory
if not os.path.exists(graph_dir):
    raise FileNotFoundError("{0} does not exist".format(graph_dir))

fns = sorted(glob.glob(os.path.join(graph_dir,"*.gml")))

def draw_graph(fn, **kwargs):
    G = nx.read_gml(fn.values[0])
    pos = nx.spring_layout(G)

    if "stochastic_block_model_graph" in fn.values[0]:
        nx.draw(G, pos=pos, node_color=list(nx.get_node_attributes(G, "block").values()), node_size=20,
                cmap=plt.cm.Dark2)
    else:
        nx.draw(G, pos=pos, node_size=20)


def parameter(G):
    str_ = " \nVertices ({0}) Edges ({1}) ".format(len(G), G.size())
    for key, val in G.graph.items():
        if key == "nb_nodes" or key == "nb_edges":
            continue
        str_ += "\n{0} ({1})".format(key.replace("_", " "), val)
    return str_


df_fns = pd.DataFrame(fns, columns=["filename"])
df_fns["index"] = np.arange(len(fns))
df_fns["label"] = df_fns.filename.apply(lambda x: x.split("/")[-1][6:])
df_fns["label"] = df_fns.label.apply(
    lambda x: x.replace("stochastic_block_model_graph", "SBM").replace("_", " ").replace(".gml", ""))
df_fns["label"] = df_fns.apply(lambda x: x.label + parameter(nx.read_gml(x.filename)), axis=1)
for ix, split in enumerate(np.array_split(df_fns, args.number_of_files)):
    g = sns.FacetGrid(split, col="label", col_wrap=4, height=5)
    g.map(draw_graph, "filename")
    axes = g.axes.flatten()
    for ax in axes:
        ax.set_title(re.sub("\d+ \n", "\n", ax.get_title()).strip("label = "))
    g.savefig(os.path.join(args.output_directory,"graph_atlas_fb{0}.pdf".format(ix)), bbox="tight_layout")
